import errno
import mysql.connector
from contextlib import contextmanager


@contextmanager
def connect_to_go():

    try:
        # cnx = mysql.connector.connect(host='mysql-amigo.ebi.ac.uk',
        #                               database='go_latest',
        #                               user='go_select',
        #                               password='amigo',
        #                               port='4085')

        cnx = mysql.connector.connect(host='spitz.lbl.gov',
                                      database='go_latest',
                                      user='go_select')

    except mysql.connector.Error as err:

        print(["Error code:", err.errno])           # error number
        print(["SQLSTATE value:", err.sqlstate])    # SQLSTATE value
        print(["Error message:", err.msg])          # error message
        print(["Error:", err])                      # errno, sqlstate, msg values
        print(["Error:", str(err)])                        # errno, sqlstate, msg values

        # if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
        #     print("Something is wrong with your user name or password")
        # elif err.errno == errorcode.ER_BAD_DB_ERROR:
        #     print("Database does not exist")
        # else:
        #     print(err)
    else:
        yield cnx

    finally:
        # close the database after use
        cnx.close()


def cursor_query(query, qargs=""):

    with connect_to_go() as cnx:

        cursor = cnx.cursor()

        cursor.execute(query,qargs)

        for query_result in cursor:
            yield query_result

        cursor.close()


if __name__ == "__main__":

    query = "SELECT \
 term.name AS superterm_name,\
 term.acc AS superterm_acc,\
 term.term_type AS superterm_type,\
 association.*,\
 gene_product.symbol AS gp_symbol,\
 gene_product.symbol AS gp_full_name,\
 dbxref.xref_dbname AS gp_dbname,\
 dbxref.xref_key AS gp_acc,\
 species.genus,\
 species.species,\
 species.ncbi_taxa_id,\
 species.common_name\
FROM term\
 INNER JOIN graph_path ON (term.id=graph_path.term1_id)\
 INNER JOIN association ON (graph_path.term2_id=association.term_id)\
 INNER JOIN gene_product ON (association.gene_product_id=gene_product.id)\
 INNER JOIN species ON (gene_product.species_id=species.id)\
 INNER JOIN dbxref ON (gene_product.dbxref_id=dbxref.id)\
WHERE\
 term.name = 'nucleus'\
 AND\
 species.genus = 'Drosophila';"

    for item in cursor_query(query):
        print(item)

    print("Running Directly")
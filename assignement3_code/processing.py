import pandas as pd
import scipy.stats as stats
import numpy as np
import os.path
from typing import Union, List, Type

StringList = List[str]
DataFrameList = List[Type[pd.DataFrame]]

hdf5_store_location = "./data/current_df_store.h5"


def get_filtered_df(run_analysis_again: bool = False, save_analysis: bool = True) -> Type[pd.DataFrame]:
    """ Function to call and get the analysed DataFrame
    This function also avoids recomputing the DataFrame each time but checks and returns the saved version from a HDF5 store

    Might add functionality for version control of the dataframe

    :param run_analysis_again: This forces the analysis to run again, to be used in case of
                                changes that need to be integrated with another file using the DataFrame
    :param save_analysis: This sets the flag whether the new analysis should be stored in the HDF5 store
    :return: Returns the Analysed DataFrame
    """

    if not run_analysis_again:
        if os.path.isfile(hdf5_store_location):
            store = pd.HDFStore(hdf5_store_location)
            return store['filtered_df']
        else:
            print("No saved analysis file, running the analysis")
            return do_analysis(save_analysis)
    else:
        return do_analysis(save_analysis)


def sig_level_cl(p_value: float) -> str:
    """
    Function to assign labels to various significances

    :param p_value: A float of the p-value
    :return label: Returns the significance label string
    """

    label = "NaN"

    if p_value > 0.05:
        label = "ns (>0.05)"
    elif 0.01 < p_value <= 0.05:
        label = "* (<0.05)"
    elif 0.001 < p_value <= 0.01:
        label = "** (<0.01)"
    elif p_value <= 0.001:
        label = "*** (<0.001)"

    return label


def mw_test(group1, group2):
    for (_, row_g1), (_, row_g2) in zip(group1.iterrows(), group2.iterrows()):
        yield stats.mannwhitneyu(row_g1, row_g2)[1]


def diff_calc(x, y):
    return np.divide(np.absolute(np.subtract(x, y)), np.mean((x, y), axis=0)) * 100


def f_test_calc(std1_column, std2_column):

    for std1, std2 in zip(std1_column, std2_column):
        lower = min(list((std1, std2)))
        higher = max(list((std1, std2)))

        if higher != 0:
            F = (lower ** 2) / (higher ** 2)
            yield stats.f.cdf(F, 5, 5)
        else:
            yield 1


def get_column_indices(search_strings: Union[str, StringList],
                       data: Type[pd.DataFrame]) -> Union[List, List[List]]:

    # check which regime to work in either its just one string or a list of strings
    if isinstance(search_strings, list):

        # if list has only one element
        if len(search_strings) == 1:

            # return just as if it were a single string
            return get_column_indices(search_strings[0], data)

        # if list has more elements do recursion
        elif len(search_strings) == 2:
            return [get_column_indices(search_strings[0], data), get_column_indices(search_strings[1], data)]

        # if list has more elements do recursion
        elif len(search_strings) > 2:

            return [*get_column_indices(search_strings[:-1], data), get_column_indices(search_strings[-1], data)]
        else:
            print("Something went wrong")

    else:
        return data.columns.map(lambda column_name: search_strings in column_name)


def do_analysis(save_to_hdf: bool = False):
    # first import the data, to a main DataFrame
    main_df = pd.read_csv('./data/01_countsWTvsKO_v2.txt', delim_whitespace=True)

    # main_df["chrom"] = main_df["chrom"].astype('category')

    # get the wildtype and experimental columns
    wt_cols, ex_cols = get_column_indices(["wt", "het"], main_df)

    # filters out the completely zero rows
    filtered_df = main_df[main_df.iloc[:, wt_cols & ex_cols].apply(lambda row: any(row), raw=True, axis=1)]

    g_expression = filtered_df.iloc[:, np.logical_or(wt_cols, ex_cols)]
    iqr = stats.iqr(g_expression)
    median = np.median(g_expression)

    g_expression_normalized = g_expression.applymap(lambda val: 1 / (1 + np.exp(-(val-median)/(iqr*1.35))))

    # calculate summary statistics (mean and std for both wt and het
    filtered_df = filtered_df.assign(wt_mean=lambda x: x.iloc[:, wt_cols].mean(axis=1, numeric_only=True),
                                     wt_std=lambda x: x.iloc[:, wt_cols].std(axis=1, numeric_only=True),
                                     ex_mean=lambda x: x.iloc[:, ex_cols].mean(axis=1, numeric_only=True),
                                     ex_std=lambda x: x.iloc[:, ex_cols].std(axis=1, numeric_only=True))

    filtered_df=filtered_df.assign(ttest_2amp=lambda x: stats.ttest_ind(x.iloc[:, wt_cols],
                                                                        x.iloc[:, ex_cols],
                                                                        axis=1,
                                                                        equal_var=False)[1])

    filtered_df = filtered_df.assign(mean_diff=lambda x: np.subtract(x.loc[:, "ex_mean"].values,
                                                                     x.loc[:, "wt_mean"].values))

    filtered_df = filtered_df.assign(mannwhit_test=lambda x: [*mw_test(x.iloc[:, wt_cols],
                                                                       x.iloc[:, ex_cols])])

    filtered_df = filtered_df.assign(f_test=lambda x: [*f_test_calc(x.loc[:, "wt_std"],
                                                                    x.loc[:, "ex_std"])])

    filtered_df = filtered_df.assign(levene_test=lambda x: [stats.levene(wt, ex)[1]
                                                            for wt, ex in zip(x.iloc[:, wt_cols].values,
                                                                              x.iloc[:, ex_cols].values)])

    filtered_df = filtered_df.assign(
        diff_mean_ratio=lambda x: diff_calc(x.loc[:, "wt_mean"].values, x.loc[:, "ex_mean"].values),
        diff_std_ratio=lambda x: diff_calc(x.loc[:, "wt_std"].values, x.loc[:, "ex_std"].values))

    filtered_df = filtered_df.assign(ttest_sig=lambda x: [sig_level_cl(pvalue)
                                                          for pvalue in
                                                          x.loc[:, "ttest_2amp"].values],
                                     mwtest_sig=lambda x: [sig_level_cl(pvalue)
                                                           for pvalue in
                                                           x.loc[:, "mannwhit_test"].values],
                                     ftest_sig=lambda x: [sig_level_cl(pvalue)
                                                          for pvalue in
                                                          x.loc[:, "f_test"].values],
                                     levenetest_sig=lambda x: [sig_level_cl(pvalue)
                                                               for pvalue in
                                                               x.loc[:, "levene_test"].values],)

    filtered_df = filtered_df.assign(type_of_diff=lambda x: ["Upregulated" if diff >= 0 else "Downregulated"
                                                             for diff in x.loc[:, "mean_diff"].values])

    if save_to_hdf:
        # save it to the current datastore
        filtered_df.to_hdf(hdf5_store_location, key="filtered_df", format='table')

    return filtered_df


if __name__ == "__main__":
    # set this to True if you want to rerun the analysis in case something was changed
    test = get_filtered_df(run_analysis_again=True, save_analysis=False)
    print("Analysis completed!")

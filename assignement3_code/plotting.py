import pandas as pd
import scipy.stats as stats
import numpy as np
import seaborn as sns; sns.set_style("ticks")
import matplotlib.pyplot as plt
from assignement3_code.processing import get_filtered_df

processed_df = pd.DataFrame(get_filtered_df())


def filter_data(column_name: str, sig_level: float, working_dataframe=processed_df):
    return working_dataframe[working_dataframe.loc[:, column_name] < sig_level]


def filter_combine_or(column_names_list: list, sig_level_list: list, working_dataframe=processed_df):
    masks_list = []
    for column_name, sig_level in zip(column_names_list,sig_level_list):
        cdx = working_dataframe.loc[:, column_name] < sig_level
        masks_list.append(cdx)

    return working_dataframe[np.any(np.array(masks_list), axis=0)]


def filter_combine_and(column_names_list: list, sig_level_list: list, working_dataframe=processed_df):
    masks_list = []
    for column_name, sig_level in zip(column_names_list,sig_level_list):
        cdx = working_dataframe.loc[:, column_name] < sig_level
        masks_list.append(cdx)

    return working_dataframe[np.all(np.array(masks_list), axis=0)]


def Figure1():

    g = sns.FacetGrid(filter_data("f_test", 0.05),
                      col="type_of_diff",
                      hue="ftest_sig")

    g = (g.map(plt.scatter, "diff_mean_ratio", "diff_std_ratio").add_legend()
         .set_axis_labels("Difference between means (in %)",
                          "Difference between standard deviations (in %)")
         .set_titles("{col_name}"))

    g._legend.set_title("Statistical significance (p-value)\n of the F-test:")

    plt.show()


def Figure8():
    # Create a figure instance, and the two subplots
    fig = plt.figure()
    ax1 = fig.add_subplot(1, 2, 1)
    ax2 = fig.add_subplot(1, 2, 2)

    plt1 = sns.lmplot('diff_mean_ratio',  # Horizontal axis
                      'diff_std_ratio',  # Vertical axis
                      data=filter_data("ttest_2amp", 0.05),  # Data source
                      fit_reg=False,  # Don't fix a regression line
                      hue="ttest_sig")  # S marker size


def Figure2():

    sns.lmplot('f_test',  # Horizontal axis
               'levene_test',  # Vertical axis
               data=processed_df,  # Data source
               hue="mwtest_sig",
               fit_reg=False)  # Don't fix a regression line

    # Set title
    plt.title('Plotting differences')

    # Set x-axis label
    plt.xlabel('f-test p-values')

    #set limits
    plt.xlim([0, 0.5])

    # Set y-axis label
    plt.ylabel('Levene p-values')

    plt.show()


def Figure9():

    sns.lmplot('ttest_2amp',  # Horizontal axis
               'mannwhit_test',  # Vertical axis
               data=processed_df,  # Data source
               hue="ftest_sig",
               fit_reg=False)  # Don't fix a regression line

    # Set title
    plt.title('Plotting differences')

    # Set x-axis label
    plt.xlabel('t-Test p-values')

    #set limits
    # plt.xlim([0, 1])
    # plt.ylim([0, 1])

    # Set y-axis label
    plt.ylabel('Mann-Whitney p-values')

    plt.show()


def Figure3():
    sns.lmplot('diff_mean_ratio',  # Horizontal axis
               'diff_std_ratio',  # Vertical axis
               data=filter_data("mannwhit_test", 0.01),  # Data source
               hue="ttest_sig",
               fit_reg=False)  # Don't fix a regression line

    # Set title
    plt.title('Plotting differences \n(Mann-Whitney test p-value < 0.01)')

    # Set x-axis label
    plt.xlabel('Difference between means (in %)')

    # Set y-axis label
    plt.ylabel('Difference between standard deviations (in %)')

    plt.show()


def Figure4():
    sns.lmplot('diff_mean_ratio',  # Horizontal axis
               'diff_std_ratio',  # Vertical axis
               data=filter_data("ttest_2amp", 0.01, filter_data("mannwhit_test", 0.01)),  # Data source
               hue="type_of_diff",
               fit_reg=False)  # Don't fix a regression line

    # Set title
    plt.title('Plotting differences \n(Mann-Whitney test p-value < 0.01)')

    # Set x-axis label
    plt.xlabel('Difference between means (in %)')

    # Set y-axis label
    plt.ylabel('Difference between standard deviations (in %)')

    plt.show()


def Figure5():

    column_names = ["mannwhit_test", "ttest_2amp", "levene_test", "f_test"]
    sig_levels = [0.01, 0.01, 0.01, 0.01]

    filter_combine_or(column_names, sig_levels)

    sns.lmplot('diff_mean_ratio',  # Horizontal axis
               'diff_std_ratio',  # Vertical axis
               data=filter_combine_or(column_names, sig_levels),  # Data source
               hue="type_of_diff",
               fit_reg=False)  # Don't fix a regression line

    # Set title
    plt.title('Plotting differences \n(Mann-Whitney test p-value < 0.01)')

    # Set x-axis label
    plt.xlabel('Difference between means (in %)')

    # Set y-axis label
    plt.ylabel('Difference between standard deviations (in %)')

    plt.show()


def Figure6():

    column_names = ["mannwhit_test", "ttest_2amp", "levene_test", "f_test"]
    sig_levels = [0.05, 0.05, 0.05, 0.05]

    sns.lmplot('diff_mean_ratio',  # Horizontal axis
               'diff_std_ratio',  # Vertical axis
               data=filter_combine_and(column_names, sig_levels),  # Data source
               hue="type_of_diff",
               fit_reg=False,
               scatter_kws={"s": 80})  # Don't fix a regression line

    # Set title
    plt.title('Plotting differences')

    # Set x-axis label
    plt.xlabel('Difference between means (in %)')

    # Set y-axis label
    plt.ylabel('Difference between standard deviations (in %)')

    plt.show()


def Figure7():

    sns.lmplot('mannwhit_test',  # Horizontal axis
               'levene_test',  # Vertical axis
               data=processed_df,  # Data source
               hue="type_of_diff",
               fit_reg=False,
               scatter_kws={"s": 10})  # Don't fix a regression line

    # Set title
    plt.title('Plotting differences')

    plt.ylim([0, 0.5])

    # Set x-axis label
    plt.xlabel('Mann-Whitney test p-values')

    # Set y-axis label
    plt.ylabel('Levene test p-values')

    plt.show()


def display_fig(fig_num: int) -> None:
    if fig_num == 1:
        Figure1()
    elif fig_num == 2:
        Figure2()
    elif fig_num == 3:
        Figure3()
    elif fig_num == 4:
        Figure4()
    elif fig_num == 5:
        Figure5()
    elif fig_num == 6:
        Figure6()
    elif fig_num == 7:
        Figure7()
    elif fig_num == 9:
        Figure9()


if __name__ == "__main__":
    display_fig(6)
